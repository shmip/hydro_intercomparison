% startup file for GlaDS example model runs:
set(0, 'DefaultAxesFontSize',14)
addpath('~/glads_matlab/')
% this m-file adds all the paths from needed for running the model:
load_glads_paths();

addpath('~/model_runs/intercomparsion/input_functions/source/matlab', '~/model_runs/intercomparsion/input_functions/topography/matlab')
addpath(pwd)
meshnr = 4
